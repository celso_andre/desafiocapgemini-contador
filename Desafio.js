function countWords(){
    const phrase = 'Não acho que quem ganhar ou quem perder nem quem ganhar nem perder vai ganhar ou perder vai todo mundo perder';
    var wordCounts = {}

    const phraseArray = phrase.split(" ");
    const map = new Map();

    for (const palavra of phraseArray) {

      let count = map.get(palavra) || 0;

      map.set(palavra, count+1)
    }
    wordCounts = Object.fromEntries(map);

    console.log(wordCounts)
}